/*jslint browser: true, continue: true, newcap: true, plusplus: true, unparam: true, todo: true, white: true */

/**
 * @author Mihai Pistol
 */
(function () {
    "use strict";

    function GPSTracker () {
    }

    /**
     * @function
     * @public
     * @memberOf {GPSTracker}
     * @param {Function} successHandler
     * @param {Function} failureHandler
     * @returns {undefined}
     */
    GPSTracker.prototype.getStatus = function (successHandler, failureHandler) {
        window.cordova.exec(successHandler, failureHandler, "GPSTracker", "getStatus", []);
    };

    /**
     * @function
     * @public
     * @memberOf {GPSTracker}
     * @param {String} bundle.cwnumm
     * @param {String} bundle.library
     * @param {String} bundle.enviroment
     * @param {Function} successHandler
     * @param {Function} failureHandler
     * @returns {undefined}
     */
    GPSTracker.prototype.register = function (bundle, successHandler, failureHandler) {
        window.cordova.exec(successHandler, failureHandler, "GPSTracker", "register", [bundle.cwnumm, bundle.library, bundle.enviroment]);
    };

    /**
     * @function
     * @public
     * @memberOf {GPSTracker}
     * @param {Function} successHandler
     * @param {Function} failureHandler
     * @returns {undefined}
     */
    GPSTracker.prototype.unregister = function (successHandler, failureHandler) {
        window.cordova.exec(successHandler, failureHandler, "GPSTracker", "unregister", []);
    };

    GPSTracker.install = function () {
        if (!window.plugins) {
            window.plugins = {};
        }
        window.plugins.GPSTracker = new GPSTracker();
        return window.plugins.GPSTracker;
    };

    window.cordova.addConstructor(GPSTracker.install);
}());
