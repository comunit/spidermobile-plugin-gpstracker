package android.nl.comunit.spidermobile.plugin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.nl.comunit.spidermobile.plugin.gps.tracker.GPSTracker;
import android.nl.comunit.spidermobile.plugin.gps.tracker.GPSTrackerService;
import android.os.Bundle;

public class TestActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getSharedPreferences(GPSTracker.SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .edit()
            .putBoolean(GPSTracker.TRACKER_ACTIVE, true)
            .putString(GPSTracker.CWNUMM, "202")
            .putString(GPSTracker.LIBRARY, "SPR")
            .putString(GPSTracker.ENVIROMENT, "4")
            .commit();
        startService(new Intent(this, GPSTrackerService.class));
    }
}
