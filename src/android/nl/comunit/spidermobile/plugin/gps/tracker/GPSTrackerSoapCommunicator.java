package nl.comunit.spidermobile.plugin.gps.tracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalDate;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

/**
 *
 * @author Mihai Pistol
 */
public class GPSTrackerSoapCommunicator {

    public static final String SOAP_WSDL = "http://ws.postnlcargo.nl:8018/axis2/services/DriverAppService?wsdl";
    public static final String SOAP_ACTION = "http://ws.postnlcargo.nl:8018/axis2/services/DriverAppService/SetCoordinate";
    public static final String SOAP_NAMESPACE = "m:";
    public static final String OBJECT_REQUEST = "CoordinateRequest";
    public static final String ATTRIBUTE_ENVIRONMENT = "Environment";
    public static final String ATTRIBUTE_LIBRARY = "Library";
    public static final String PROPERTY_CWNUMM = "CWNUMM";
    public static final String PROPERTY_TIME = "Time";
    public static final String OBJECT_COORDINATES = "Coordinates";
    public static final String PROPERTY_COORDINATES_XCOO = "XCOO";
    public static final String PROPERTY_COORDINATES_YCOO = "YCOO";

    private final Context context;
    private final String cwnumm;
    private final String library;
    private final String enviroment;

    public GPSTrackerSoapCommunicator (Context context) {
        this.context = context;
        SharedPreferences sharedPreferences = context.getSharedPreferences(GPSTracker.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.cwnumm = sharedPreferences.getString(GPSTracker.CWNUMM, null);
        this.library = sharedPreferences.getString(GPSTracker.LIBRARY, null);
        this.enviroment = sharedPreferences.getString(GPSTracker.ENVIROMENT, null);
    }

    public boolean isOnline () {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public void executeCall (Location location) {
        SoapSerializationEnvelope envelope = buildRequest(location);
        if (isOnline()) {
            new NetworkCommunicationCallResponse(envelope).execute();
        }
    }

    private String getStringDate (Location location) {
        // 2014-07-16T15:56:55.048+02:00
        SimpleDateFormat dateFormatTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String time = dateFormatTime.format(location != null ? new Date(location.getTime()) : new Date());
        time = time.substring(0, time.length() - 2) + ":" + time.substring(time.length() - 2, time.length());
        return time;
    }

    private SoapSerializationEnvelope buildRequest (Location location) {
        SoapObject objectCoordinateRequest = new SoapObject("", SOAP_NAMESPACE + OBJECT_REQUEST);
        objectCoordinateRequest.addAttribute(ATTRIBUTE_LIBRARY, library);
        objectCoordinateRequest.addAttribute(ATTRIBUTE_ENVIRONMENT, enviroment);
        objectCoordinateRequest.addAttribute("xmlns:m", "http://app.driver.webservice.namespace");
        //CWNUMM
        PropertyInfo propertyCwnumm = new PropertyInfo();
        propertyCwnumm.setName(SOAP_NAMESPACE + PROPERTY_CWNUMM);
        propertyCwnumm.setType(String.class);
        propertyCwnumm.setValue(cwnumm);
        objectCoordinateRequest.addProperty(propertyCwnumm);
        //Time
        PropertyInfo propertyTime = new PropertyInfo();
        propertyTime.setName(SOAP_NAMESPACE + PROPERTY_TIME);
        propertyTime.setType(String.class);
        propertyTime.setValue(getStringDate(location));
        objectCoordinateRequest.addProperty(propertyTime);
        //Coordinates
        SoapObject objectCoordinates = new SoapObject("", SOAP_NAMESPACE + OBJECT_COORDINATES);
        objectCoordinateRequest.addSoapObject(objectCoordinates);
        //XCOO
        PropertyInfo propertyXcoo = new PropertyInfo();
        propertyXcoo.setName(SOAP_NAMESPACE + PROPERTY_COORDINATES_XCOO);
        propertyXcoo.setType(Double.class);
        propertyXcoo.setValue(location != null ? location.getLatitude() : 0D);
        objectCoordinates.addProperty(propertyXcoo);
        //YCOO
        PropertyInfo propertyYcoo = new PropertyInfo();
        propertyYcoo.setName(SOAP_NAMESPACE + PROPERTY_COORDINATES_YCOO);
        propertyYcoo.setType(Double.class);
        propertyYcoo.setValue(location != null ? location.getLongitude() : 0D);
        objectCoordinates.addProperty(propertyYcoo);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.env = SoapEnvelope.ENV;
        envelope.setAddAdornments(false);
        envelope.implicitTypes = true;
        envelope.dotNet = true;
        envelope.setOutputSoapObject(objectCoordinateRequest);
        new MarshalDate().register(envelope);
        new MarshalFloat().register(envelope);
        return envelope;
    }

    private Object parseResponse (Object response) {
        return null;
    }

    private class NetworkCommunicationCallResponse extends AsyncTask<Void, Void, Object> {

        private final SoapSerializationEnvelope envelope;

        private NetworkCommunicationCallResponse (SoapSerializationEnvelope envelope) {
            this.envelope = envelope;
        }

        @Override
        protected Object doInBackground (Void... params) {
            HttpTransportSE transport = new HttpTransportSE(SOAP_WSDL);
            transport.debug = true;
            Object response = null;
            try {
                transport.call(SOAP_ACTION, envelope);
                response = GPSTrackerSoapCommunicator.this.parseResponse(envelope.getResponse());
            } catch (IOException e) {
                Logger.getLogger("GPSTracker").log(Level.SEVERE, "exception caught", e);
            } catch (XmlPullParserException e) {
                Logger.getLogger("GPSTracker").log(Level.SEVERE, "exception caught", e);
            } catch (NullPointerException npe) {
                return null;
            }
            String requestDump = transport.requestDump;
            if (requestDump != null) {
                Logger.getLogger("GPSTracker").log(Level.INFO, requestDump);
            }
            String responseDump = transport.responseDump;
            if (responseDump != null) {
                Logger.getLogger("GPSTracker").log(Level.INFO, responseDump);
            }
            return response;
        }

        @Override
        protected void onPostExecute (Object result) {
            super.onPostExecute(result);
        }
    }
}
