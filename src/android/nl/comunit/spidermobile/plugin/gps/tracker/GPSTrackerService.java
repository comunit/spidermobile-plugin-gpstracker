package nl.comunit.spidermobile.plugin.gps.tracker;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import java.util.Date;

/**
 * @author Mihai Pistol
 */
public class GPSTrackerService extends Service implements LocationListener {

    public static final int NOTIFICATION_GPS_DISABLEAD = 0;

    private static boolean active = false;
    private static long lastTime = new Date().getTime();
    private GPSTrackerSoapCommunicator communicator;

    @Override
    public IBinder onBind (Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand (Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (!active) {
            active = true;
            communicator = new GPSTrackerSoapCommunicator(this);
            registerLocationRequest();
        }
        return Service.START_STICKY;
    }

    public void onLocationChanged (Location location) {
        if (lastTime <= (location.getTime() - GPSTracker.INTERVAL)) {
            communicator.executeCall(location);
            lastTime = location.getTime();
        }
    }

    public void onStatusChanged (String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled (String provider) {
        clearNotificationGPS();
    }

    public void onProviderDisabled (String provider) {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))) {
            displayNotificationGPS();
        }
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
        ((LocationManager) getSystemService(Context.LOCATION_SERVICE)).removeUpdates(this);
        clearNotificationGPS();
        active = false;
    }

    private void registerLocationRequest () {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, GPSTracker.INTERVAL, GPSTracker.DISTANCE, this);
        }
        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, GPSTracker.INTERVAL, GPSTracker.DISTANCE, this);
        }
    }

    private void displayNotificationGPS () {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification notification = new Notification.Builder(this)
            .setContentTitle("GPS is not enabled.")
            .setContentText("Tap on the notification to enable.")
            .setSmallIcon(getApplicationInfo().icon)
            .setContentIntent(pendingIntent)
            .setOngoing(true)
            .getNotification();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_GPS_DISABLEAD, notification);
    }

    private void clearNotificationGPS () {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_GPS_DISABLEAD);
    }
}
