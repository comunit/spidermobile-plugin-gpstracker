package nl.comunit.spidermobile.plugin.gps.tracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

/**
 * @author Mihai Pistol
 */
public class GPSTrackerBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive (Context context, Intent intent) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(GPSTracker.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(GPSTracker.TRACKER_ACTIVE, false)) {
            context.startService(new Intent(context, GPSTrackerService.class));
        }
    }
}
