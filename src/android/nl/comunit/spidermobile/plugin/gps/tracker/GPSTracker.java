package nl.comunit.spidermobile.plugin.gps.tracker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * @author Mihai Pistol
 */
public class GPSTracker extends CordovaPlugin {

    public static final String SHARED_PREFERENCES = "nl.comunit.spidermobile.plugin.gps.GPSTracker.SHARED_PREFERENCES";
    public static final String TRACKER_ACTIVE = "nl.comunit.spidermobile.plugin.gps.GPSTracker.TRACKER_ACTIVE";

    // The actual driver and truck id, something like “SpiderDriver MW 123 CW 456”
    // where 123 is the driver id and 456 is the truck id.
    // at the moment is just truck id
    public static final String CWNUMM = "cwnumm";
    public static final String LIBRARY = "library";
    public static final String ENVIROMENT = "enviroment";

    public static final long INTERVAL = 60000L;
    public static final float DISTANCE = 0F;

    private Context context = null;
    private SharedPreferences sharedPreferences = null;

    @Override
    public void initialize (CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Logger.getLogger("GPSTracker").log(Level.INFO, "initialized()");
        context = cordova.getActivity().getApplicationContext();
        sharedPreferences = context.getSharedPreferences(GPSTracker.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (sharedPreferences.getBoolean(GPSTracker.TRACKER_ACTIVE, false)) {
            context.startService(new Intent(context, GPSTrackerService.class));
        }
    }

    @Override
    public boolean execute (String action, JSONArray data, CallbackContext callback) throws JSONException {
        boolean result = false;
        if ("getStatus".equals(action)) {
            callback.success(getStatus() ? "true" : "false");
            result = true;
        } else if ("register".equals(action)) {
            register(data);
            result = true;
        } else if ("unregister".equals(action)) {
            unregister();
            result = true;
        }
        return result;
    }

    public boolean getStatus () {
        Logger.getLogger("GPSTracker").log(Level.INFO, "initialized()");
        return sharedPreferences.getBoolean(GPSTracker.TRACKER_ACTIVE, false);
    }

    public void register (JSONArray args) throws JSONException {
        Logger.getLogger("GPSTracker").log(Level.INFO, "register()");
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(CWNUMM, args.getString(0));
        edit.putString(LIBRARY, args.getString(1));
        edit.putString(ENVIROMENT, args.getString(2));
        edit.putBoolean(TRACKER_ACTIVE, true);
        edit.commit();
        context.startService(new Intent(context, GPSTrackerService.class));
    }

    public void unregister () {
        Logger.getLogger("GPSTracker").log(Level.INFO, "unregister()");
        context.stopService(new Intent(context, GPSTrackerService.class));
        sharedPreferences.edit().putBoolean(TRACKER_ACTIVE, false).commit();
    }
}
